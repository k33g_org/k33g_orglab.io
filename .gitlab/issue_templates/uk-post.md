<!-- drag'n drop a nice picture -->

<!-- write a kind of introduction -->


## First §


<!-- Youtube Link
[![Title](http://img.youtube.com/vi/37PNaArR4mc/0.jpg)](http://www.youtube.com/watch?v=37PNaArR4mc "Title")
-->


<!-- the end -->

<hr>

> 👋
> - If you loved this "post" (or not), don't forget to use the emojis reactions
> - Don't hesitate to add comments and/or ask questions
> - You can subscribe to the [Rss feed](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues.atom?state=opened)

<!--
 - don't forget to add labels to this issue
 - don't forget the related issues
 -->

/label ~"To Do" ~"🇬🇧"
/assign me
/confidential
/milestone %"Waiting ..."
