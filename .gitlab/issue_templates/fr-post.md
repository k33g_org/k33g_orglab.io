<!-- drag'n drop a nice picture -->

<!-- write a kind of introduction -->


## Premier §

<!-- Youtube Link
[![Title](http://img.youtube.com/vi/37PNaArR4mc/0.jpg)](http://www.youtube.com/watch?v=37PNaArR4mc "Title")
-->





<!-- the end -->

<hr>

> 👋
> - Si vous avez aimé (ou pas) ce "post" n'hésitez pas à utiliser les emojis pour réagir
> - N'hésitez pas non plus à laisser des commentaires ou poser des questions
> - Enfin, vous pouvez utiliser ce lien [Rss feed](https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues.atom?state=opened) pour vous abonner à ce blog à base d'issues

<!--
 - don't forget to add labels to this issue
 - don't forget the related issues
 -->

/label ~"To Do" ~"🇫🇷"
/assign me
/confidential
/milestone %"Waiting ..."
