import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <section class="section">
    <div class="container">
      <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
          <li><a href="https://gitlab.com/k33g_org/k33g_org.gitlab.io"> 🌍 k33g_org.gitlab.io</a></li>
          <!--
          <li><a href="https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues">📙 Posts</a></li>
          -->
          <li><a href="https://gitlab.com/k33g">👋 @k33g</a></li>
          <li><a href="https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues.atom?state=opened">📝 Subscribe to RSS feed</a></li>
        </ul>
      </nav>
    </div>
  </section>
`

class LinksBar extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('links-bar', LinksBar)
