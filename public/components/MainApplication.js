import {Style, Template, Ossicle, Router} from '../js/Ossicle.js'
import './MainTitle.js'
import './LinksBar.js'
import './LastIssues.js'
import './IssuesList.js'
import './TabsBar.js'

const template = Template.html`
<div>
  <main-title></main-title>
  <links-bar></links-bar>

  <div class="container">
    <tabs-bar></tabs-bar>

    <section id="last" class="section" style="display:none">
      <last-issues title="🎉 Last 12 news"></last-issues>
    </section>

    <section id="posts" class="section" style="display:none">
      <issues-list title="📝 Posts"></issues-list>
    </section>

    <section id="conversations" class="section" style="display:none">
      <issues-list title="📻 Conversations"></issues-list>
    </section>
  </div>

</div>
`

class MainApplication extends Ossicle {

  constructor() {
    super()
    /*
      - clone the template and add it to the shadow dom
      - add the style sheet(s) to the "adoptedStyleSheets" of the shadow dom
    */
    this.setup(template.clone(), [Style.sheets.bulma])

    this.projectId = 22154718
    this.gitlabApiUrl = "https://gitlab.com/api/v4"
    this.issuesUrl = `${this.gitlabApiUrl}/projects/${this.projectId}/issues?per_page=100`
  }

  connectedCallback() {
    // the main application is listening
    this.addEventListener('refresh-issues-list', this.onMessage)

    this.getIssuesList()

    // start the router and call `onRouterEvent` at every `hash` event
    // the firs time the router triggers a `load` event
    Router.start(message => this.onRouterEvent(message))
  }

  onRouterEvent(message) {
    /*
    if(message.type==='load') {}
    if(message.type==='hashchange') {}
    */
    let tabId = message.location === "/" ? "last" : message.location.slice(1)

    this.$classes("section").forEach(section => section.style.display="none")

    this.$id(tabId).style.display = "block"

    this.$("tabs-bar").activate(tabId)
  }


  getIssuesList() {
    fetch(this.issuesUrl)
      .then(response => response.json())
      .then(data => this.sendEvent('refresh-issues-list', { issues: data }))
      .catch(error => {
        console.log("😡 getIssuesList", error)
      })
  }

  onMessage(message) {

    if(message.type==='refresh-issues-list') {
      let issuesList = message.detail.issues.filter(issue => issue.labels.length>0 && issue.labels.includes("conversation")==false)
      let conversationsList = message.detail.issues.filter(issue => issue.labels.includes("conversation"))

      issuesList.sort((a,b) => {return new Date(b.updated_at) - new Date(a.updated_at)})

      this.$('last-issues').refreshList(issuesList.slice(0, 12))
      this.$all('issues-list')[0].refreshList(issuesList)
      this.$all('issues-list')[1].refreshList(conversationsList)
    }
  }

}

customElements.define('main-application', MainApplication)
