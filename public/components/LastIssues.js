import {Style, Template, Fragment, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div class="container">
    <h1 class="title">
      <!-- -->
    </h1>
    <div class="columns is-multiline"></div>
  </div>
`

export class LastIssues extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  formatDate(dateString) {
    const options = { year: "numeric", month: "long", day: "numeric" }
    return new Date(dateString).toLocaleDateString(undefined, options)
  }

  refreshList(issues) {

    this.$class("columns").innerHTML =
      issues.map(issue => {
        return Fragment.html`
          <div class="column is-one-quarter">
            <div class="card">
              <header class="card-header">
                <p class="card-header-title" style="height:70px">
                  ${issue.title}
                </p>
              </header>
              <div class="card-content">
                <div class="content">
                  created at: ${this.formatDate(issue.created_at)} | updated at: ${this.formatDate(issue.updated_at)}
                  <br>
                  <a href="${issue.web_url}">Read...</a>
                  <hr>
                  ${issue.labels.map(label => Fragment.html`<span class="tag is-link">${label}</span>`).join(" ")}
                </div> <!-- content -->
              </div> <!-- card-content -->
            </div> <!-- card -->
          </div> <!-- column -->
        `
      }).join("")
  }

  connectedCallback() {
    this.$class("title").innerHTML = this.getAttribute("title")
  }

}

customElements.define('last-issues', LastIssues)
