import {Style, Template, Ossicle} from '../js/Ossicle.js'

const mainTitle = "📝 Issues are the new blog"
const subTitle = "All my thoughts 🤔 in 🇫🇷 or in 🇬🇧"

const template = Template.html`
  <section class="hero is-bold is-info">
    <div class="hero-body">
      <div class="container">
        <h1 class="title is-1">
          ${mainTitle}
        </h1>
        <h2 class="subtitle">
          ${subTitle}
        </h2>
      </div>
    </div>
  </section>
`

class MainTitle extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('main-title', MainTitle)
